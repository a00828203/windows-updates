#===============================================================================
#
#          FILE: Updateblacklist.ps1
# 
#  BEFORE USAGE: 1) In the script below, fill in the items for $ftpserver (Should be the FTP server's external IP so the script works outside), $port, $url, and $url2
#				 2) In the command under USAGE, fill in path to script and credential file
#				 3) Run script with command prompt command under USAGE
#         USAGE: In command prompt or Powershell, run the following with admin rights: Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; New-Item -ItemType Directory -Force -Path C:\Program Files (x86)\Update Scheduler\_SyncFiles; unblock-file -path C:\Program Files (x86)\Update Scheduler\_SyncFiles\updateblacklist.ps1; powershell C:\Program Files (x86)\Update Scheduler\_SyncFiles\updateblacklist.ps1; set-executionpolicy -executionpolicy restricted �force
#
#   DESCRIPTION: Windows Updates - the Powershell way
#              
#       OPTIONS: ---
#  REQUIREMENTS: Powershell 5.1 or higher (Included with Windows 10 1607/Server 2016, can be installed on W7/2008 and higher)
#          BUGS: ---
#         NOTES: Reference for this Powershell Module at https://www.powershellgallery.com/packages/PSWindowsUpdate/2.1.1.2
#                Reference for this GPO at https://getadmx.com/?Category=Windows_10_2016&Policy=Microsoft.Policies.WindowsUpdate::AutoUpdateCfg
#        AUTHOR: Edmond Wong
#       CREATED: 05/01/2020
#      REVISION: 0.9.4
#===============================================================================

$path = 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\blacklist.txt'
$folder = 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\'
$registrypath = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
$registrypath2 = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$registrypath3 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU\'
$registrypath4 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$name1 = 'AutoInstallMinorUpdates'
$value1 = '0'
$name2 = 'NoAutoUpdate'
$value2 = '0'
$name3 = 'AUOptions'
$value3 = '4'
$name4 = 'ScheduledInstallDay'
$value4 = '5'
$name5 = 'ScheduledInstallTime'
$value5 = '2'
$name6 = 'ScheduledInstallFirstWeek'
$value6 = '1'
#$name7 = 'ScheduledInstallThirdWeek'
#$value7 = '1'
$name8 = 'NoAutoRebootWithLoggedOnUsers'
$value8 = '1'
$name9 = 'ExcludeWUDriversInQualityUpdate'
$value9 = '1'

#Set registry entries to schedule updates at 2AM on the first Thursday of the month (Uncomment item #7 for installs on the first and third thursdays of the month)
Echo "Check and set registry entries per update schedule..."
if(!(Test-Path $registrypath)) {
New-Item -Path $registryPath -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}

if(!(Test-Path $registrypath2)) {
New-Item -Path $registrypath2 -Force | Out-Null
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}

#Install and Import Windows Update Powershell Module
Echo "Install and Import Windows Update Powershell Module..."
Import-Module Powershellget
Install-Module -Name PSWindowsUpdate -force
Import-Module -Name PSWindowsUpdate

#Hide updates from blacklist
Echo "Hide updates from blacklist..."
$hidelist = [IO.File]::ReadAllText("$path")
Hide-WindowsUpdate -KBArticleID $hidelist -hide -confirm:$false