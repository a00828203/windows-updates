#===============================================================================
#
#          FILE: Updatestatus.ps1
# 
#  BEFORE USAGE: 1) In the script below, fill in the items for $ftpserver (Should be the FTP server's external IP so the script works outside), $port, $url, and $url2
#				 2) In the command under USAGE, fill in path to script and credential file
#				 3) Run script with command prompt command under USAGE
#         USAGE: In command prompt or Powershell, run the following with admin rights: Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; New-Item -ItemType Directory -Force -Path C:\Program Files (x86)\Update Scheduler\_SyncFiles; unblock-file -path C:\Program Files (x86)\Update Scheduler\_SyncFiles\updatestatus.ps1; powershell C:\Program Files (x86)\Update Scheduler\_SyncFiles\updatestatus.ps1; set-executionpolicy -executionpolicy restricted �force
#
#   DESCRIPTION: Windows Updates - the Powershell way
#              
#       OPTIONS: ---
#  REQUIREMENTS: Powershell 5.1 or higher (Included with Windows 10 1607/Server 2016, can be installed on W7/2008 and higher)
#          BUGS: ---
#         NOTES: Reference for this Powershell Module at https://www.powershellgallery.com/packages/PSWindowsUpdate/2.1.1.2
#                Reference for this GPO at https://getadmx.com/?Category=Windows_10_2016&Policy=Microsoft.Policies.WindowsUpdate::AutoUpdateCfg
#        AUTHOR: Edmond Wong
#       CREATED: 05/01/2020
#      REVISION: 0.9.4
#===============================================================================

$folder = 'C:\Program Files (x86)\Update Scheduler\_SyncFiles'
$hostname = $env:computername
$registrypath = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
$registrypath2 = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$registrypath3 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU\'
$registrypath4 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$name1 = 'AutoInstallMinorUpdates'
$value1 = '0'
$name2 = 'NoAutoUpdate'
$value2 = '0'
$name3 = 'AUOptions'
$value3 = '4'
$name4 = 'ScheduledInstallDay'
$value4 = '5'
$name5 = 'ScheduledInstallTime'
$value5 = '2'
$name6 = 'ScheduledInstallFirstWeek'
$value6 = '1'
#$name7 = 'ScheduledInstallThirdWeek'
#$value7 = '1'
$name8 = 'NoAutoRebootWithLoggedOnUsers'
$value8 = '1'
$name9 = 'ExcludeWUDriversInQualityUpdate'
$value9 = '1'
$log = "C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdateStatus-$hostname.txt"
$log2 = "C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdateSuccess-$hostname.txt"
$log3 = "C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdateFailure-$hostname.txt"
$eventlogdate = (get-date).AddDays(-30)
$success = Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object -Line | Select-Object -ExpandProperty Lines
$failure = Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object -Line | Select-Object -ExpandProperty Lines

#Create log file titled as the computer's hostname - hostname, domain, computer info, Utils folder directory listing & timestamps, WU registry values, hidden updates, and installed updates - to log path
Echo "Create log file with computer and registry info..."
systeminfo | findstr /B /C:"Host Name" /C:"Domain" /C:"OS Name" /C:"OS Version" /C:"System Model" /C:"Total Physical Memory" /C:"System Boot Time" > $log
wmic bios get serialnumber >> $log
dir $folder >> $log
echo $name1 >> $log
REG QUERY $registrypath3 /v $name1 >> $log
echo $name2 >> $log
REG QUERY $registrypath3 /v $name2 >> $log
echo $name3 >> $log
REG QUERY $registrypath3 /v $name3 >> $log
echo $name4 >> $log
REG QUERY $registrypath3 /v $name4 >> $log
echo $name5 >> $log
REG QUERY $registrypath3 /v $name5 >> $log
echo $name6 >> $log
REG QUERY $registrypath3 /v $name6 >> $log
#echo $name7 >> $log
#REG QUERY $registrypath3 /v $name7 >> $log
echo $name8 >> $log
REG QUERY $registrypath3 /v $name8 >> $log
echo $name9 >> $log
REG QUERY $registrypath4 /v $name9 >> $log
Get-WindowsUpdate -IsHidden >> $log
dism /online /get-packages /format:table >> $log
Get-InstalledModule >> $log

#Check logs for successful updates in the last 30 days, if none don't add anything to the log
#Event ID 19: Installation successful: Windows successfully installed the following update
Echo "Check logs for successful updates in the last 30 days..."
Echo "Windows Updates Installed: $success" > $log2
Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log2

#Check logs for failed updates in the last 30 days, if none don't add anything to the log
#Event ID 20: Installation failure: Windows failed to install the following update with error %1: %2.
#Event ID 31: Windows Update failed to download an update.
#Event ID 16: Unable to connect: Windows is unable to connect to the Automatic Updates service and therefore cannot download and install updates
#Event ID 25: Windows Update failed to check for updates
Echo "Check logs for failed updates in the last 30 days..."
Echo "Windows Updates Failed: $failure" > $log3
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log3
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='31';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log3
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='16';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log3
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='25';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log3