README - v0.9.4

File Hierarchy Assumptions Diagram

C:\Program Files (x86)\Update Scheduler
 _SyncFiles
  blacklist.txt 
  Updateblacklist.ps1
  Updatestatus.ps1

 _Upload
  WindowsUpdatestatus-$hostname.txt
  WindowsUpdateSuccess-$hostname.txt
  WindowsUpdateFailure-$hostname.txt
  
 
Blacklist.txt 
-Formatting is ['KB4489873', 'KB4489883', 'KB4489884'] without brackets

------

Launcher commands:
-Can run in CMD or Powershell
-They don't upload or download anything (so I don't need put credentials in the scripts
-Need to be run as commands rather than scripts, because they setup the prerequisites for running the powershell scripts

------

Updateblacklist.ps1
-Checks Windows Update registry entries and sets them if they are not set 
-Installs/updates the Windows Update Powershell Module
-Hides updates from C:\Program Files (x86)\Update Scheduler\_SyncFiles\blacklist.txt; 
-Ensure current directory is C:\Program Files (x86)\Update Scheduler\_SyncFiles if relative path in command below is desired

Relative Path version launcher (Assumes current dir is already C:\Program Files (x86)\Update Scheduler\_SyncFiles):

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; powershell unblock-file -path Updateblacklist.ps1; powershell .\Updateblacklist.ps1; set-executionpolicy -executionpolicy restricted –force

Relative Path version launcher (includes CD command to C:\Program Files (x86)\Update Scheduler\_SyncFiles):

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; cd 'C:\Program Files (x86)\Update Scheduler\_SyncFiles'; powershell unblock-file -path Updateblacklist.ps1; powershell .\Updateblacklist.ps1; set-executionpolicy -executionpolicy restricted –force

Fullpath Launcher command: 

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; unblock-file -path 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\Updateblacklist.ps1'; powershell 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\Updateblacklist.ps1'; set-executionpolicy -executionpolicy restricted –force

------

Updatestatus.ps1
-Creates the following log files:
 -C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdatestatus-$hostname.txt
 -C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdateSuccess-$hostname.txt
 -C:\Program Files (x86)\Update Scheduler\_Upload\WindowsUpdateFailure-$hostname.txt

Relative Path version launcher (Assumes current dir is already C:\Program Files (x86)\Update Scheduler\_SyncFiles):

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; powershell unblock-file -path Updatestatus.ps1; powershell .\Updatestatus.ps1; set-executionpolicy -executionpolicy restricted –force

Relative Path version launcher (includes CD command to C:\Program Files (x86)\Update Scheduler\_SyncFiles):

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; cd 'C:\Program Files (x86)\Update Scheduler\_SyncFiles'; powershell unblock-file -path Updatestatus.ps1; powershell .\Updatestatus.ps1; set-executionpolicy -executionpolicy restricted –force

Fullpath Launcher command: 

Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; unblock-file -path 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\Updatestatus.ps1'; powershell 'C:\Program Files (x86)\Update Scheduler\_SyncFiles\Updatestatus.ps1'; set-executionpolicy -executionpolicy restricted –force