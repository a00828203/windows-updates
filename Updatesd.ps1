#===============================================================================
#
#          FILE: updates.ps1
# 
#         USAGE: powershell set-executionpolicy -executionpolicy unrestricted; import-module PowerShellGet; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; cmd /c echo a | powershell Find-PackageProvider -Name 'Nuget' -ForceBootstrap -IncludeDependencies; New-Item -ItemType Directory -Force -Path C:\Utils; (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/UkS5n0P2', 'C:\Utils\updates.ps1'); powershell C:\Utils\updates.ps1; set-executionpolicy -executionpolicy restricted
# 	
#   DESCRIPTION: Windows Updates the Powershell way
#              
#       OPTIONS: ---
#  REQUIREMENTS: Windows 10 Pro 1703 or later
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Edmond Wong
#  ORGANIZATION: ---
#       CREATED: 10/29/2019 3:00
#      REVISION: ---
#===============================================================================

#For workgroup computers, updates are installed on the 3rd Friday of the month at 3AM
#Reference for this Powershell Module at http://woshub.com/pswindowsupdate-module/
#Reference for this GPO at 

$urls = @()
$item = "" | Select path,outpath,name
$item.path = 'https://pastebin.com/raw/BKCNWPdJ'
$item.outpath = 'C:\Utils\'
$item.name = 'blacklist.txt'
$urls = $urls + $item
$path = 'C:\Utils\blacklist.txt'
$registrypath = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
$registrypath2 = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$name1 = 'AutoInstallMinorUpdates'
$value1 = '0'
$name2 = 'NoAutoUpdate'
$value2 = '0'
$name3 = 'AUOptions'
$value3 = '4'
$name4 = 'ScheduledInstallDay'
$value4 = '6'
$name5 = 'ScheduledInstallTime'
$value5 = '3'
$name6 = 'ScheduledInstallThirdWeek'
$value6 = '1'
$name7 = 'NoAutoRebootWithLoggedOnUsers'
$value7 = '1'
$name8 = 'ExcludeWUDriversInQualityUpdate'
$value8 = '1'

#Set registry entries to schedule updates if they aren't already
if(!(Test-Path $registrypath)) {
New-Item -Path $registryPath -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 `
-PropertyType DWORD -Force | Out-Null}

if(!(Test-Path $registrypath2)) {
New-Item -Path $registrypath2 -Force | Out-Null
New-ItemProperty -Path $registrypath2 -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath2 -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}

#Download blacklist and set it as the list of updates to be hidden, blacklist format is "KB4517389", "KB4519573"
ForEach ( $item in $urls) {
$file = ($item.outpath) + ($item.name)
(New-Object System.Net.WebClient).DownloadFile($item.path, $file)}
$hidelist = [IO.File]::ReadAllText("$path")

#Install and Import Windows Update Powershell Module
Import-Module Powershellget
Install-Module -Name PSWindowsUpdate
Import-Module -Name PSWindowsUpdate

#Hide updates from blacklist
Hide-WindowsUpdate -KBArticleID $hidelist -hide -confirm:$false

#Create log file titled as the computer's hostname - hostname, domain, computer info, Utils folder directory listing & timestamps, WU registry values, hidden updates, and installed updates - to log path
systeminfo | findstr /B /C:"Host Name" /C:"Domain" /C:"OS Name" /C:"OS Version" /C:"System Model" /C:"Total Physical Memory" /C:"System Boot Time" > $log
wmic bios get serialnumber >> $log
dir $folder >> $log
echo $name1 >> $log
REG QUERY $registrypath3 /v $name1 >> $log
echo $name2 >> $log
REG QUERY $registrypath3 /v $name2 >> $log
echo $name3 >> $log
REG QUERY $registrypath3 /v $name3 >> $log
echo $name4 >> $log
REG QUERY $registrypath3 /v $name4 >> $log
echo $name5 >> $log
REG QUERY $registrypath3 /v $name5 >> $log
echo $name6 >> $log
REG QUERY $registrypath3 /v $name6 >> $log
echo $name7 >> $log
REG QUERY $registrypath3 /v $name7 >> $log
echo $name8 >> $log
REG QUERY $registrypath4 /v $name8 >> $log
Get-WindowsUpdate -IsHidden >> $log
dism /online /get-packages /format:table >> $log}


