#===============================================================================
#
#          FILE: Updatesdftp.ps1
# 
#  BEFORE USAGE: 1) In the script below, fill in the items for $ftpserver (Should be the FTP server's external IP so the script works outside), $port, $url, and $url2
#				 2) In the command under USAGE, fill in path to script and credential file
#				 3) Run script with command prompt command under USAGE
#         USAGE: In command prompt or Powershell, run the following with admin rights: Powershell set-executionpolicy -executionpolicy bypass; powershell [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell import-module PowerShellGet; Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Force; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; New-Item -ItemType Directory -Force -Path C:\Utils; Invoke-WebRequest 'https://pastebin.com/raw/4XCahCSw' -OutFile 'C:\Utils\updatesdftp.ps1'; Invoke-WebRequest 'https://pastebin.com/raw/eExqEgVc' -OutFile 'C:\Utils\file.txt'; unblock-file -path C:\Utils\updatesdftp.ps1; powershell C:\Utils\updatesdftp.ps1; set-executionpolicy -executionpolicy restricted �force
#
#   DESCRIPTION: Windows Updates - the Powershell way
#              
#       OPTIONS: ---
#  REQUIREMENTS: Powershell 5.1 or higher (Included with Windows 10 1607/Server 2016, can be installed on W7/2008 and higher)
#          BUGS: ---
#         NOTES: Reference for this Powershell Module at https://www.powershellgallery.com/packages/PSWindowsUpdate/2.1.1.2
#                Reference for this GPO at https://getadmx.com/?Category=Windows_10_2016&Policy=Microsoft.Policies.WindowsUpdate::AutoUpdateCfg
#        AUTHOR: Edmond Wong
#       CREATED: 04/25/2020
#      REVISION: 0.9.3
#===============================================================================

$ftpserver = '184.68.128.226'
$port = '2221'
$url = 'https://bitbucket.org/a00828203/windows-updates/raw/8dfc5990d50ebae6811742254dbb35be3ffcc5f1/blacklist.txt'
$url2 = 'https://bitbucket.org/a00828203/windows-updates/raw/8dfc5990d50ebae6811742254dbb35be3ffcc5f1/WinSCP.zip'
$path = 'C:\Utils\blacklist.txt'
$path2 = 'C:\Utils\winscp.zip'
$folder = 'C:\Utils\'
$registrypath = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
$registrypath2 = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$registrypath3 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU\'
$registrypath4 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$name1 = 'AutoInstallMinorUpdates'
$value1 = '0'
$name2 = 'NoAutoUpdate'
$value2 = '0'
$name3 = 'AUOptions'
$value3 = '4'
$name4 = 'ScheduledInstallDay'
$value4 = '5'
$name5 = 'ScheduledInstallTime'
$value5 = '2'
$name6 = 'ScheduledInstallFirstWeek'
$value6 = '1'
#$name7 = 'ScheduledInstallThirdWeek'
#$value7 = '1'
$name8 = 'NoAutoRebootWithLoggedOnUsers'
$value8 = '1'
$name9 = 'ExcludeWUDriversInQualityUpdate'
$value9 = '1'
$hostname = $env:computername
$date = Get-Date -format "yyyy-MM-dd HH-mm-ss"
$domain = wmic computersystem get domain | findstr local
$log = "C:\Utils\$hostname-$date-${domain}log.txt"
$winscp = 'C:\Utils\winscp.com'
$username = Get-Content -Path C:\Utils\file.txt | Select-Object -First 1
$password = Get-Content -Path C:\Utils\file.txt | Select-Object -Last 1
$credentials = $username + ':' + $password
$fingerprint = '4e-10-09-79-22-d2-87-58-e4-07-b7-32-79-d0-10-a4-76-12-c2-e7'
$winscpfile1 = "C:\Utils\WinSCP.com"
$winscp1exists = Test-Path $winscpfile1
$winscpfile2 = "C:\Utils\WinSCP.exe"
$winscp2exists = Test-Path $winscpfile2
$eventlogdate = (get-date).AddDays(-30)
$success = Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object -Line | Select-Object -ExpandProperty Lines
$failure = Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object -Line | Select-Object -ExpandProperty Lines

#Set registry entries to schedule updates at 2AM on the first Thursday of the month (Uncomment item #7 for installs on the first and third thursdays of the month)
Echo "Check and set registry entries per update schedule..."
if(!(Test-Path $registrypath)) {
New-Item -Path $registryPath -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}

if(!(Test-Path $registrypath2)) {
New-Item -Path $registrypath2 -Force | Out-Null
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}

#If WinSCP.exe & WinSCP.com are present, re-download the blacklist; if any of these two are missing, then re-download the blacklist, download WinSCP.zip, and extract it, replacing all existing components.  Delete the downloaded zip(s).  Set the blacklist as the list of updates to be hidden.
Echo "Download the blacklist and WinSCP, set updates to be hidden..."
if ($winscp1exists -eq $True -and $winscp2exists -eq $True) {
Invoke-WebRequest $url -OutFile $path}
else {
Invoke-WebRequest $url -OutFile $path
Invoke-WebRequest $url2 -OutFile $path2
Expand-Archive -Path $path2 -DestinationPath $folder -force
Remove-Item c:\utils\*.zip -force}
$hidelist = [IO.File]::ReadAllText("$path")

#Install and Import Windows Update Powershell Module
Echo "Install and Import Windows Update Powershell Module..."
Import-Module Powershellget
Install-Module -Name PSWindowsUpdate -force
Import-Module -Name PSWindowsUpdate

#Hide updates from blacklist
Echo "Hide updates from blacklist..."
Hide-WindowsUpdate -KBArticleID $hidelist -hide -confirm:$false

#Create log file titled as the computer's hostname - hostname, domain, computer info, Utils folder directory listing & timestamps, WU registry values, hidden updates, and installed updates - to log path
Echo "Create log file with computer and registry info..."
systeminfo | findstr /B /C:"Host Name" /C:"Domain" /C:"OS Name" /C:"OS Version" /C:"System Model" /C:"Total Physical Memory" /C:"System Boot Time" > $log
wmic bios get serialnumber >> $log
dir $folder >> $log
echo $name1 >> $log
REG QUERY $registrypath3 /v $name1 >> $log
echo $name2 >> $log
REG QUERY $registrypath3 /v $name2 >> $log
echo $name3 >> $log
REG QUERY $registrypath3 /v $name3 >> $log
echo $name4 >> $log
REG QUERY $registrypath3 /v $name4 >> $log
echo $name5 >> $log
REG QUERY $registrypath3 /v $name5 >> $log
echo $name6 >> $log
REG QUERY $registrypath3 /v $name6 >> $log
#echo $name7 >> $log
#REG QUERY $registrypath3 /v $name7 >> $log
echo $name8 >> $log
REG QUERY $registrypath3 /v $name8 >> $log
echo $name9 >> $log
REG QUERY $registrypath4 /v $name9 >> $log
Get-WindowsUpdate -IsHidden >> $log
dism /online /get-packages /format:table >> $log
Get-InstalledModule >> $log

#Check logs for successful updates in the last 30 days, if none don't add anything to the log
#Event ID 19: Installation successful: Windows successfully installed the following update
Echo "Check logs for successful updates in the last 30 days..."
Echo "Windows Updates Installed: $success" >> $log 
Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log

#Check logs for failed updates in the last 30 days, if none don't add anything to the log
#Event ID 20: Installation failure: Windows failed to install the following update with error %1: %2.
#Event ID 31: Windows Update failed to download an update.
#Event ID 16: Unable to connect: Windows is unable to connect to the Automatic Updates service and therefore cannot download and install updates
#Event ID 25: Windows Update failed to check for updates
Echo "Check logs for failed updates in the last 30 days..."
Echo "Windows Updates Failed: $failure" >> $log
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='31';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='16';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='Microsoft-Windows-WindowsUpdateClient/Operational';ID='25';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log

#Upload log file to FTP server via Winscp
Echo "Upload log file to FTP server..."
cd c:\utils; cmd.exe /c $winscp /command "open ftpes://$credentials;fingerprint=${fingerprint}@${ftpserver}:$port/" "put *log.txt" "exit"