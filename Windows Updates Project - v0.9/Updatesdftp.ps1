#===============================================================================
#
#          FILE: Updatesdftp.ps1
# 
#  BEFORE USAGE: 1) In the script below, fill in the items for $ftpserver (Should be the FTP server's external IP so the script works outside), $port, $url, and $url2
#				 2) In the command under USAGE, fill in path to script and credential file
#				 3) Run script with command prompt command under USAGE
#         USAGE: In command prompt or Powershell, run the following with admin rights: powershell set-executionpolicy -executionpolicy bypass; import-module PowerShellGet; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; cmd /c echo a | powershell Find-PackageProvider -Name 'Nuget' -ForceBootstrap -IncludeDependencies; New-Item -ItemType Directory -Force -Path C:\Utils; (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/1BRfun8V', 'C:\Utils\updatesdftp.ps1'); (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/xpjTqq0i', 'C:\Utils\file.txt'); powershell C:\Utils\updatesdftp.ps1
#
#   DESCRIPTION: Windows Updates - the Powershell way
#              
#       OPTIONS: ---
#  REQUIREMENTS: Windows 10 Pro 1703 or later
#          BUGS: ---
#         NOTES: Reference for this Powershell Module at https://www.powershellgallery.com/packages/PSWindowsUpdate/2.1.1.2
#                Reference for this GPO at https://getadmx.com/?Category=Windows_10_2016&Policy=Microsoft.Policies.WindowsUpdate::AutoUpdateCfg
#        AUTHOR: Edmond Wong
#       CREATED: 04/03/2020
#      REVISION: 0.9
#===============================================================================

$ftpserver = '192.168.6.97'
$port = '2221'
$url = 'https://bitbucket.org/a00828203/windows-updates/raw/8dfc5990d50ebae6811742254dbb35be3ffcc5f1/blacklist.txt'
$url2 = 'https://bitbucket.org/a00828203/windows-updates/raw/8dfc5990d50ebae6811742254dbb35be3ffcc5f1/WinSCP.zip'
$path = 'C:\Utils\blacklist.txt'
$path2 = 'C:\Utils\winscp.zip'
$folder = 'C:\Utils\'
$registrypath = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
$registrypath2 = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$registrypath3 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU\'
$registrypath4 = 'HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\'
$name1 = 'AutoInstallMinorUpdates'
$value1 = '0'
$name2 = 'NoAutoUpdate'
$value2 = '0'
$name3 = 'AUOptions'
$value3 = '4'
$name4 = 'ScheduledInstallDay'
$value4 = '5'
$name5 = 'ScheduledInstallTime'
$value5 = '2'
$name6 = 'ScheduledInstallFirstWeek'
$value6 = '1'
#$name7 = 'ScheduledInstallThirdWeek'
#$value7 = '1'
$name8 = 'NoAutoRebootWithLoggedOnUsers'
$value8 = '1'
$name9 = 'ExcludeWUDriversInQualityUpdate'
$value9 = '1'
$hostname = $env:computername
$date = Get-Date -format "yyyy-MM-dd HH-mm-ss"
$domain = wmic computersystem get domain | findstr local
$log = "C:\Utils\$hostname-$date-${domain}log.txt"
$winscp = 'C:\Utils\winscp.com'
$username = Get-Content -Path C:\Utils\file.txt | Select-Object -First 1
$password = Get-Content -Path C:\Utils\file.txt | Select-Object -Last 1
$credentials = $username + ':' + $password
$fingerprint = '4e-10-09-79-22-d2-87-58-e4-07-b7-32-79-d0-10-a4-76-12-c2-e7'
$winscpfile1 = "C:\Utils\WinSCP.com"
$winscp1exists = Test-Path $winscpfile1
$winscpfile2 = "C:\Utils\WinSCP.exe"
$winscp2exists = Test-Path $winscpfile2

#Set registry entries to schedule updates at 2AM on the first Thursday of the month (Uncomment item #7 for installs on the first and third thursdays of the month)
if(!(Test-Path $registrypath)) {
New-Item -Path $registryPath -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath -Name $name1 -Value $value1 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name2 -Value $value2 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registryPath -Name $name3 -Value $value3 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name4 -Value $value4 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name5 -Value $value5 `
-PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name6 -Value $value6 `
-PropertyType DWORD -Force | Out-Null
#New-ItemProperty -Path $registrypath -Name $name7 -Value $value7 ` -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $registrypath -Name $name8 -Value $value8 `
-PropertyType DWORD -Force | Out-Null}

if(!(Test-Path $registrypath2)) {
New-Item -Path $registrypath2 -Force | Out-Null
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}
else {
New-ItemProperty -Path $registrypath2 -Name $name9 -Value $value9 `
-PropertyType DWORD -Force | Out-Null}

#If WinSCP.exe & WinSCP.com are present, re-download the blacklist; if any of these two are missing, then re-download the blacklist, download WinSCP.zip, and extract it, replacing all existing components.  Delete the downloaded zip(s).  Set the blacklist as the list of updates to be hidden.
if ($winscp1exists -eq $True -and $winscp2exists -eq $True) {
Invoke-WebRequest $url -OutFile $path}
else {
Invoke-WebRequest $url -OutFile $path
Invoke-WebRequest $url2 -OutFile $path2
Expand-Archive -Path $path2 -DestinationPath $folder -force
Remove-Item c:\utils\*.zip -force}
$hidelist = [IO.File]::ReadAllText("$path")

#Install and Import Windows Update Powershell Module
Import-Module Powershellget
Install-Module -Name PSWindowsUpdate
Import-Module -Name PSWindowsUpdate

#Hide updates from blacklist
Hide-WindowsUpdate -KBArticleID $hidelist -hide -confirm:$false

#Create log file titled as the computer's hostname - hostname, domain, computer info, Utils folder directory listing & timestamps, WU registry values, hidden updates, and installed updates - to log path
systeminfo | findstr /B /C:"Host Name" /C:"Domain" /C:"OS Name" /C:"OS Version" /C:"System Model" /C:"Total Physical Memory" /C:"System Boot Time" > $log
wmic bios get serialnumber >> $log
dir $folder >> $log
echo $name1 >> $log
REG QUERY $registrypath3 /v $name1 >> $log
echo $name2 >> $log
REG QUERY $registrypath3 /v $name2 >> $log
echo $name3 >> $log
REG QUERY $registrypath3 /v $name3 >> $log
echo $name4 >> $log
REG QUERY $registrypath3 /v $name4 >> $log
echo $name5 >> $log
REG QUERY $registrypath3 /v $name5 >> $log
echo $name6 >> $log
REG QUERY $registrypath3 /v $name6 >> $log
echo $name7 >> $log
REG QUERY $registrypath3 /v $name7 >> $log
echo $name9 >> $log
REG QUERY $registrypath4 /v $name9 >> $log
Get-WindowsUpdate -IsHidden >> $log
dism /online /get-packages /format:table >> $log
Get-InstalledModule >> $log

#Upload log file to FTP server via Winscp
cd c:\utils; cmd.exe /c $winscp /command "open ftpes://$credentials;fingerprint=${fingerprint}@${ftpserver}:$port/" "put *log.txt" "exit"