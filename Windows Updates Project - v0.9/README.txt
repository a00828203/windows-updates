README - Windows Updates Project - v0.9

---------

1) Script Variants:

Updatesdftp.ps1 - Download script and blacklist from internet/FTP server to C:\Utils and run, upload log file to FTP server - This is the current version planned.

Also available:
Updates.ps1 = Assume the script and blacklist are downloaded to C:\Utils and run
Updatesl.ps1 - Assume the script and blacklist are downloaded to C:\Utils and run if laptop
Updatesd.ps1 - Download script and blacklist from internet to C:\Utils and run

2) BEFORE USAGE: A) In Updatesdftp.ps1, review (and change if needed) the items for $ftpserver (Should be the FTP server's external IP so the script works outside), $port, $url, and $url2
		 B) In the "Launcher" command below, fill in path to script "Updatesdftp.ps1" and credential file "file.txt"
		 C) Run this "Launcher" command to download the script from a website - no need to transfer script over manually - please note script download link and filepath below, for Updatesd or Updatesdftp.ps1

Launcher as one line (For running manually or via Task Scheduler with cmd or powershell), no current directory requirements:

powershell set-executionpolicy -executionpolicy bypass; import-module PowerShellGet; Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; cmd /c echo a | powershell Find-PackageProvider -Name 'Nuget' -ForceBootstrap -IncludeDependencies; New-Item -ItemType Directory -Force -Path C:\Utils; (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/1BRfun8V', 'C:\Utils\updatesdftp.ps1'); (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/xpjTqq0i', 'C:\Utils\file.txt'); powershell C:\Utils\updatesdftp.ps1

This downloads the script and installs all prerequisites required to run it, then runs the script.  Please note that the script may appear to freeze at times, but rest assured it is running - it may take 2-4 minutes to run.

Separated line by line:

powershell set-executionpolicy -executionpolicy bypass; 
import-module PowerShellGet; 
Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted; 
cmd /c echo a | powershell Find-PackageProvider -Name 'Nuget' -ForceBootstrap -IncludeDependencies; 
New-Item -ItemType Directory -Force -Path C:\Utils; (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/1BRfun8V', 'C:\Utils\updatesdftp.ps1'); (New-Object System.Net.WebClient).DownloadFile('https://pastebin.com/raw/xpjTqq0i', 'C:\Utils\file.txt'); 
powershell C:\Utils\updatesdftp.ps1

3) Reverting changes to your Registry - if desired, run this to delete the added registry entries added by the script.  This is one command, so you can copy and paste the entire block in at once and it will do everything required to undo the changes of the script.  This is not necessary until you're done testing.

reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v AutoInstallMinorUpdates /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v NoAutoUpdate /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v AUOptions /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v ScheduledInstallDay /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v ScheduledInstallTime /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v ScheduledInstallFirstWeek /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v NoAutoRebootWithLoggedOnUsers /f & reg delete HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate /v ExcludeWUDriversInQualityUpdate /f

If the option to install updates on the third week of the month was allowed, will also need this:
HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v ScheduledInstallThirdWeek /f & reg delete 