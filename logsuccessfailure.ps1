﻿$log = 'c:\users\auron\desktop\log.txt'

$eventlogdate = (get-date).AddDays(-30)
$success = Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object –Line | Select-Object -ExpandProperty Lines
$failure = Get-WinEvent -FilterHashTable @{LogName='System';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | Measure-Object –Line | Select-Object -ExpandProperty Lines

#Check logs for successful updates in the last 30 days, if none don't add anything to the log
#Event ID 19: Installation successful: Windows successfully installed the following update
Echo "Windows Updates Installed: $success" >> $log 
Get-WinEvent -FilterHashTable @{LogName='System';ID='19';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log

#Check logs for failed updates in the last 30 days, if none don't add anything to the log
#Event ID 20: Installation failure: Windows failed to install the following update with error %1: %2.
#Event ID 31: Windows Update failed to download an update.
#Event ID 16: Unable to connect: Windows is unable to connect to the Automatic Updates service and therefore cannot download and install updates
#Event ID 25: Windows Update failed to check for updates
Echo "Windows Updates Failed: $failure" >> $log
Get-WinEvent -FilterHashTable @{LogName='System';ID='20';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='System';ID='31';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='System';ID='16';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log
Get-WinEvent -FilterHashTable @{LogName='System';ID='25';StartTime = $eventlogdate;} -maxevents 10 -erroraction 'silentlycontinue' | out-string -Width 300 >> $log